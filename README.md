# Artefatos de Arquitetura

[Application Facade - Components - Integrations](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Application%20Facade%20-%20Components%20-%20Integrations.jpg)

[Cronograma Desenvolvimento MVP1](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Cronograma%20GroupCamper.jpg)

[Interface de usuário e lógica de apresentação](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Interface%20de%20usua%CC%81rio%20e%20lo%CC%81gica%20de%20apresentac%CC%A7a%CC%83o.jpg)

[Jornadas dos usuários](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Jornadas%20dos%20usua%CC%81rios.jpg)

[Matriz Esforço Valor](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Matriz%20Esforc%CC%A7o%20Valor.jpg)

[Persona Convidada](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Persona%20Convidada.jpg)

[Persona Organizadora](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Persona%20Organizadora.jpg)

[Visão do MVP - Elevator Pitch](https://bitbucket.org/tiagoesanto/groupcamper/src/master/Visa%CC%83o%20do%20MVP%20-%20Elevator%20Pitch.jpg)