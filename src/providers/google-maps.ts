import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Connectivity } from './connectivity-service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { google } from 'google-maps';

declare var google: any;

@Injectable()
export class GoogleMaps {

  mapElement: any;
  map: any;
  mapInitialised: boolean = false;
  mapLoaded: any;
  mapLoadedObserver: any;
  currentMarker: any;
  apiKey: string = "AIzaSyAq2idFcBx5VlMjbJYkW274z7AzwrWlaIU";

  constructor(public connectivityService: Connectivity, public geolocation: Geolocation) {

  }

  init(mapElement: any): Promise<any> {

    this.mapElement = mapElement;

    return this.loadGoogleMaps();

  }

  loadGoogleMaps(): Promise<any> {

    return new Promise((resolve) => {

      if(typeof google == "undefined" || typeof google.maps == "undefined"){

        console.log("Google maps JavaScript needs to be loaded.");
        this.disableMap();

        if(this.connectivityService.isOnline()){

          window['mapInit'] = () => {

            this.initMap().then(() => {
              resolve(true);
            });

            this.enableMap();
          }

          // let script = document.createElement("script");
          // script.id = "googleMaps";

          // if(this.apiKey){
          //   script.src = 'https://maps.google.com/maps/api/js?key=' + this.apiKey + '&callback=mapInit&libraries=places';
          // } else {
          //   script.src = 'https://maps.google.com/maps/api/js?callback=mapInit';       
          // }

          // document.body.appendChild(script);  

        } 
      } else {

        if(this.connectivityService.isOnline()){
          this.initMap().then(() => {
              this.mapLoaded = true;
              this.enableMap();
              resolve(true);
          });
        }
        else {
          this.disableMap();

          resolve(true);
        }

      }

      this.addConnectivityListeners();

    });

  }

  initMap(): Promise<any> {

    this.mapInitialised = true;

    return new Promise((resolve) => {

      let mapOptions = {
        center: {lat: -30.03, lng: -51.23},
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      this.map = new google.maps.Map(this.mapElement, mapOptions);
      resolve(true);

      // this.geolocation.getCurrentPosition().then((position) => {

      //   let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      //   let mapOptions = {
      //     center: latLng,
      //     zoom: 15,
      //     mapTypeId: google.maps.MapTypeId.ROADMAP
      //   }

      //   this.map = new google.maps.Map(this.mapElement, mapOptions);
      //   resolve(true);

      // });

    });

  }

  disableMap(): void {

  }

  enableMap(): void {

  }

  addConnectivityListeners(): void {

    this.connectivityService.watchOnline().subscribe(() => {

      setTimeout(() => {

        if(typeof google == "undefined" || typeof google.maps == "undefined"){
          this.loadGoogleMaps();
        } 
        else {
          if(!this.mapInitialised){
            this.initMap().then(() => {
                this.enableMap();
            });
          }else{
            this.enableMap();
          }
        }

      }, 2000);

    });

    this.connectivityService.watchOffline().subscribe(() => {

      this.disableMap();

    });

  }

}
