import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Contacts, IContactField, IContactFindOptions } from "@ionic-native/contacts/ngx";

@Component({
  selector: 'trip',
  templateUrl: './trip.page.html',
  styleUrls: ['./trip.page.scss'],
})
export class TripPage implements OnInit {
  contactsFound = [];
  selectedContacts = [];
  tripForm: FormGroup;
  htmlToAdd: string;
  constructor(private formBuilder: FormBuilder, private contacts: Contacts) { 
    this.tripForm = this.formBuilder.group({
      name: [''],
      description: [''],
      date: [''],
      origin: [''],
      destination: [''],
      stops: [''],
      invitees: ['']
      });
      this.search('');
      this.htmlToAdd = 'IT WORKED!';
  }  

  ngOnInit() {
  }

  search(q){
    const option: IContactFindOptions={
      filter: q
    }
    this.contacts.find(["displayName"], option).then(conts => {
      conts.forEach(contact => {
        this.contactsFound = conts;
      });      
    })
  }
  
  onKeyUp(ev) {
    this.search(ev.target.value);
  }

  onContactSelection(contact){
    if (this.selectedContacts.includes(contact)){
      this.selectedContacts.splice(this.selectedContacts.indexOf(contact), 1);
    }
    else{
      this.selectedContacts.push(contact);
    }
  }

}
