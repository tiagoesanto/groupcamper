import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsPageRoutingModule } from './tabs.router.module';
import { TabsPage } from './tabs.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Contacts } from "@ionic-native/contacts/ngx";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    FormsModule,
    ReactiveFormsModule, 
  ],
  exports:[
    FormsModule,
    ReactiveFormsModule 
  ],
  providers:[Contacts],
  declarations: [TabsPage]
})
export class TabsPageModule {}
