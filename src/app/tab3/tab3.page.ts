import { Component, OnInit } from '@angular/core';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import {Platform} from "@ionic/angular";
import { TripPage } from '../trip/trip.page';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page  {
  socialSharing: SocialSharing;

  tripName: string;
  tripDescription: string;
  tripDate: string;
  tripOrigin: string;
  tripDestiny: string;
  tripStop: string;

  encodedGoogleMapsLink: string;
  shareText: string;

  htmlToAdd: string;
  constructor(private facebook: Facebook, socialSharing: SocialSharing, public platform: Platform) {    
  }
  
  ionViewWillEnter(){
    this.getDataFromTab1();
  }

  getDataFromTab1(){
    this.tripName = window.localStorage.getItem("TripName").toString(); 
    this.tripDescription = window.localStorage.getItem("TripDescription").toString(); 
    var date = new Date(window.localStorage.getItem("TripDate")); 

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var minutes = date.getMinutes() > 9 ? date.getMinutes() : "0" + date.getMinutes();
    var hours = date.getHours() > 9 ? date.getHours() : "0" + date.getMinutes();

    this.tripDate = day+"/"+(monthIndex+1)+"/"+year+" "+ hours+"h "+minutes+"m";

    this.tripOrigin = window.localStorage.getItem("TripOrigin").toString();  
    this.tripDestiny = window.localStorage.getItem("TripDestiny").toString();  
    this.tripStop = window.localStorage.getItem("TripStop").toString(); 

    let rawGoogleMapsLink = "https://www.google.com/maps/dir/?api=1&origin="
    + this.tripOrigin + "&destination=" + this.tripDestiny + "&waypoints=" + this.tripStop + "&travelmode=driving";
    this.encodedGoogleMapsLink = encodeURI(rawGoogleMapsLink);

    this.shareText = this.tripName + ".\n Data: " + this.tripDate + "\n" + this.tripDescription;
  }

  onClick(){
    if(this.platform.is("cordova")){    
      window['plugins'].socialsharing.shareViaFacebook(this.shareText, null, this.encodedGoogleMapsLink, function () { console.log('share ok') }, function (errormsg) { alert(errormsg) });      
    }
  }

  onClick2(){
    if(this.platform.is("cordova")){    
      window['plugins'].socialsharing.shareViaWhatsApp(this.shareText, null, this.encodedGoogleMapsLink, function () { console.log('share ok') }, function (errormsg) { alert(errormsg) });      
    }
  }
}


