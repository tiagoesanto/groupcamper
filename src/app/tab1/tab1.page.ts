import { NgModule, OnInit } from '@angular/core';
import { Component, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GoogleMaps } from '../../providers/google-maps';
import { google } from 'google-maps';

declare var google: any;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  @ViewChild('map', null) mapElement: ElementRef;

  latitude: number;
  longitude: number;
  autocompleteService: any;
  placesService: any;
  searchDisabled: boolean;
  saveDisabled: boolean;
  location: any;

  name: string = '';
  description: string = '';
  date: string = new Date().toISOString();

  placesOrigin: any = [];
  placesDestiny: any = [];
  placesStop: any = [];

  isPlacesOriginVisible: boolean = false;
  isPlacesDestinyVisible: boolean = false;
  isPlacesStopVisible: boolean = false;

  queryOrigin: string = '';
  queryDestiny: string = '';
  queryStop: string = '';

  selectedOriginLocation: string = '';
  selectedDestinyLocation: string = '';
  selectedStopLocation: string = '';

  searchBarChangedByUs = false;

  constructor(public navCtrl: NavController, public zone: NgZone, public maps: GoogleMaps,
    public platform: Platform, public geolocation: Geolocation) {
    this.searchDisabled = true;
    this.saveDisabled = true;
  }

  ngOnInit() {
    this.loadMap();
    this.mockData();
  }

  mockData() {
    this.name = "Camping Camaquã"
    this.description = "Viagem de moto para camaquã. Será necessário levar barraca para dormir, carne para o churrasco, carvão e espetos."
    this.date = new Date(2019, 11, 15, 14, 0, 0, 0).toISOString();

    this.queryOrigin = this.selectedOriginLocation = "Av. dos Estados, 75 - São João, Porto Alegre";
    this.queryDestiny = this.selectedDestinyLocation = "Barragem do Arroio Duro, R. Cap. Jango Castro, Camaquã";
    this.queryStop = this.selectedStopLocation = "Restaurante das Cucas - BR-116 s/n, RS";
    }

  loadMap(): void {

    let mapLoaded = this.maps.init(this.mapElement.nativeElement)
      .then(() => {

        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.placesService = new google.maps.places.PlacesService(this.maps.map);
        this.searchDisabled = false;

      }).catch(error => {
        let errorMessage = error;
        alert(error)
      });

  }

  ionViewWillLeave() {
    window.localStorage.setItem("TripName", this.name);
    window.localStorage.setItem("TripDescription", this.description);
    window.localStorage.setItem("TripDate", this.date);
    window.localStorage.setItem("TripOrigin", this.selectedOriginLocation);
    window.localStorage.setItem("TripDestiny", this.selectedDestinyLocation);
    window.localStorage.setItem("TripStop", this.selectedStopLocation);
  }

  searchPlaceOrigin() {
    let searchCallBack = (predictions): void => {
      this.placesOrigin = [];

      predictions.forEach((prediction) => {
        this.placesOrigin.push(prediction);
      });
    };
    if (this.queryOrigin.length > 0) {
      this.searchPlace(this.queryOrigin, searchCallBack);
      this.isPlacesOriginVisible = true;
    } else {
      this.placesOrigin = [];
    }
  }

  searchPlaceDestiny() {
    let searchCallBack = (predictions): void => {
      this.placesDestiny = [];

      predictions.forEach((prediction) => {
        this.placesDestiny.push(prediction);
      });
    };
    if (this.queryDestiny.length > 0) {
      this.searchPlace(this.queryDestiny, searchCallBack);
      this.isPlacesDestinyVisible = true;
    } else {
      this.placesDestiny = [];
    }
  }

  searchPlaceStop() {
    let searchCallBack = (predictions): void => {
      this.placesStop = [];

      predictions.forEach((prediction) => {
        this.placesStop.push(prediction);
      });
    };

    if (this.queryStop.length > 0) {
      this.searchPlace(this.queryStop, searchCallBack);
      this.isPlacesStopVisible = true;
    } else {
      this.placesStop = [];
    }
  }

  searchPlace(query, placeFunc) {
    this.saveDisabled = true;

    let config = {
      types: ['geocode'],
      input: query
    }

    this.autocompleteService.getPlacePredictions(config, (predictions, status) => {

      if (status == google.maps.places.PlacesServiceStatus.OK && predictions) {
        placeFunc(predictions);
      }

    });
  }

  selectPlaceOrigin(place) {
    this.placesOrigin = [];
    this.queryOrigin = this.selectedOriginLocation = place.description;
  }

  selectPlaceDestiny(place) {
    this.placesDestiny = [];
    this.queryDestiny = this.selectedDestinyLocation = place.description;
  }

  selectPlaceStop(place) {
    this.placesStop = [];
    this.queryStop = this.selectedStopLocation = place.description;
  }

  addFocusOrigin() {
    if (this.placesOrigin.length > 0)
      this.isPlacesOriginVisible = true;
  }

  removeFocusOrigin() {
    if (this.placesListShouldBeVisible(this.placesOrigin, this.selectedOriginLocation, this.queryOrigin))
      this.isPlacesOriginVisible = false;
  }

  addFocusDestiny() {
    if (this.placesDestiny.length > 0)
      this.isPlacesDestinyVisible = true;
  }

  removeFocusDestiny() {
    if (this.placesListShouldBeVisible(this.placesDestiny, this.selectedDestinyLocation, this.queryDestiny))
      this.isPlacesDestinyVisible = false;
  }

  addFocusStop() {
    if (this.placesStop.length > 0)
      this.isPlacesStopVisible = true;
  }

  removeFocusStop() {
    if (this.placesListShouldBeVisible(this.placesStop, this.selectedStopLocation, this.queryStop))
      this.isPlacesStopVisible = false;
  }

  private placesListShouldBeVisible(places, selectedLocation, query) {
    return places.length == 0 || (selectedLocation.length > 0 && selectedLocation == query);
  }
}
