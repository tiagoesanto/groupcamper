import { NgModule, OnInit } from '@angular/core';
import { Component, ElementRef, ViewChild, NgZone } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GoogleMaps } from '../../providers/google-maps';
import { google } from 'google-maps';

declare var google: any;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']  
})
export class Tab2Page {
  @ViewChild('map', null) mapElement: ElementRef;
  
  autocompleteService: any;
  placesService: any;
  directionsService: any;
  directionsRenderer: any;

  constructor(public maps: GoogleMaps, private geolocation: Geolocation) {
    
  }

  ionViewWillEnter(){    
    this.loadMap();
  }  

  loadMap(): void {
    this.directionsService = new google.maps.DirectionsService();
    this.directionsRenderer = new google.maps.DirectionsRenderer();
    
    let mapLoaded = this.maps.init(this.mapElement.nativeElement).then(() => {
        
      if (this.maps.mapInitialised && this.maps.mapLoaded){
        this.autocompleteService = new google.maps.places.AutocompleteService();
        this.placesService = new google.maps.places.PlacesService(this.maps.map);
        this.directionsRenderer.setMap(this.maps.map);
        this.calculateAndDisplayRoute(this.directionsService, this.directionsRenderer);
      }
    }); 
  }

  calculateAndDisplayRoute(directionsService, directionsRenderer) {
    let tripOrigin = window.localStorage.getItem("TripOrigin");
    let tripDestiny = window.localStorage.getItem("TripDestiny");
    let tripStop = window.localStorage.getItem("TripStop");

    if (tripOrigin && tripDestiny){
      directionsService.route(
          {
            origin: {query: tripOrigin},
            destination: {query: tripDestiny},     
            waypoints: [{location: tripStop, stopover: true}],
            travelMode: 'DRIVING'
          },
          function(response, status) {
            if (status === 'OK') {
              directionsRenderer.setDirections(response);
            } else {
              window.alert('Directions request failed due to ' + status);
            }
          });
    }
  } 
}